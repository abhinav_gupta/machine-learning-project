[examples, labels] = generateFinalExamplesAndLabels('data_set\data-starplus-05680-v7.mat');
%[examples] = generateExamplesByAveragingAllImages(examples);
nFolds = 10;

dist_mat = distanceMatric(examples, examples, @euclidianDistance);

K = 5;

%c = cvpartition(labels, 'Kfold', nFolds);
c = cvpartition(labels,'leaveout');
%cv_error = zeros(1, K);
cv_mat_error = zeros(c.NumTestSets, K);

for i = 1:c.NumTestSets
    trIdx = c.training(i);
    tsIdx = c.test(i);
    trX = examples(trIdx,:);
    trY = labels(trIdx);
    tsX = examples(tsIdx,:);
    tsY = labels(tsIdx);
    
    % Used precomputed disctances.
    dist = dist_mat(trIdx, tsIdx);
    
    % KNN Prediction
    cv_mat_error(i,:) = KNNpredict(trX, trY, ...
        tsY, K, dist);
end
cv_error= mean(cv_mat_error, 1);
1 - cv_error
