function [ predictions ] = applyNBClassifier( dataset, model )
    numDataInst = size(dataset, 1);
    means = model{1};
    stdev = model{2};
    priors = model{3};
    nClasses = length(priors);
    for i = 1: nClasses
        meansPerClass = means(i, :);
        stdevPerClass = stdev(i, :);
        meanForAllInst = repmat(meansPerClass, [numDataInst, 1]);
        stdevForAllInst = repmat(stdevPerClass, [numDataInst, 1]);
        repData = dataset;
        repData = repData - meanForAllInst;
        repData = repData ./stdevForAllInst;
        repData = -1 * (repData .^ 2);
        repData = repData / 2;
        repData = repData - log(sqrt(2*pi)*stdevForAllInst);
        logMLE = sum(repData, 2);
        posterior(:,i) = logMLE + log(priors(i));
    end
    [val, score] = max(posterior, [], 2);
    predictions = score;
end
    
