function [ accuracy ] = summarizeResults( predictions, testLabels )
    accuracy = sum(logical(testLabels == predictions))/length(testLabels);

end

