
function [ examples, labels ] = generateFinalExamplesAndLabels( file_name, featureType )
load(file_name);

% collect the non-noise and non-fixation trials
trials=find([info.cond]>1);
[info1,data1,meta1]=filterDataBasedOnTrails(info,data,meta,trials);

switch featureType
    %['all_feature', 'active_roi', 'active_roi_avg', 'avg_voxel'];
    case { 'all_feature' }
        %default do nothing
    case {'active_roi'}
       % [info1, data1, meta1] = get_ROI_Voxels(info1, data1, meta1, {'CALC' 'LIPL' 'LT' 'LTRIA' 'LOPER' 'LIPS' 'LDLPFC'});
        [info1, data1, meta1] = get_ROI_Voxels(info1, data1, meta1, {'CALC'});
    case {'active_roi_avg'}
        [info1, data1, meta1] = transformIDM_avgROIVoxels(info1, data1, meta1, {'CALC' 'LIPL' 'LT' 'LTRIA' 'LOPER' 'LIPS' 'LDLPFC'});
end

%[info1, data1, meta1] = selectActiveVoxels(info1, data1, meta1, 500);
% seperate P1st and S1st trials
[infoP1,dataP1,metaP1]=filterDataBasedOnTrails(info1,data1,meta1,find([info1.firstStimulus]=='P'));
[infoS1,dataS1,metaS1]=filterDataBasedOnTrails(info1,data1,meta1,find([info1.firstStimulus]=='S'));

% seperate reading P vs S
[infoP2,dataP2,metaP2]=filterDataBasedOnTime(infoP1,dataP1,metaP1,[1:16]);
[infoP3,dataP3,metaP3]=filterDataBasedOnTime(infoS1,dataS1,metaS1,[17:32]);
[infoS2,dataS2,metaS2]=filterDataBasedOnTime(infoP1,dataP1,metaP1,[17:32]);
[infoS3,dataS3,metaS3]=filterDataBasedOnTime(infoS1,dataS1,metaS1,[1:16]);

% Using ROI's
%     [infoP2, dataP2, metaP2] = transformIDM_selectROIVoxels(infoP2, dataP2, metaP2, {'CALC' 'LIPL' 'LT' 'LTRIA' 'LOPER' 'LIPS' 'LDLPFC'});
%     [infoP3, dataP3, metaP3] = transformIDM_selectROIVoxels(infoP3, dataP3, metaP3, {'CALC' 'LIPL' 'LT' 'LTRIA' 'LOPER' 'LIPS' 'LDLPFC'});
%     [infoS2, dataS2, metaS2] = transformIDM_selectROIVoxels(infoS2, dataS2, metaS2, {'CALC' 'LIPL' 'LT' 'LTRIA' 'LOPER' 'LIPS' 'LDLPFC'});
%     [infoS3, dataS3, metaS3] = transformIDM_selectROIVoxels(infoS3, dataS3, metaS3, {'CALC' 'LIPL' 'LT' 'LTRIA' 'LOPER' 'LIPS' 'LDLPFC'});

% convert to examples
[examplesP2,labelsP2,exInfoP2]=transformIDMTOExamplesLabels(infoP2,dataP2,metaP2);
[examplesP3,labelsP3,exInfoP3]=transformIDMTOExamplesLabels(infoP3,dataP3,metaP3);
[examplesS2,labelsS2,exInfoS2]=transformIDMTOExamplesLabels(infoS2,dataS2,metaS2);
[examplesS3,labelsS3,exInfoS3]=transformIDMTOExamplesLabels(infoS3,dataS3,metaS3);

% combine examples and create labels.  Label 'picture' 1, label 'sentence' 2.
examplesP=[examplesP2;examplesP3];
examplesS=[examplesS2;examplesS3];
labelsP=ones(size(examplesP,1),1);
labelsS=ones(size(examplesS,1),1)+1;
examples=[examplesP;examplesS];
labels=[labelsP;labelsS];

end

