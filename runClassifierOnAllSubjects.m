function [ subVsClassfrVsFeatureAccuracy ] = runClassifierOnAllSubjects( data_dir, classifiers )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%files  = dir('data_set/*.mat')
files  = dir(strcat(data_dir ,'/data-starplus-04847-v7.mat'));
%files  = dir(strcat(data_dir ,'/*.mat'));
%accuracy = zeros(length(files),1);
%for i = 1:length(files)
features = classifiers.features;
classfr_types = classifiers.type;
ntypes = length(classfr_types);
nfeatures = length(features);
nfiles = length(files);
%nfiles = 1;
subVsClassfrVsFeatureAccuracy = zeros(ntypes, nfeatures, nfiles);

for i = 1:nfiles
%for i = 1:1
   f_fullpath = strcat(data_dir, '/', files(i).name);
   fprintf('==running on file %s\n', files(i).name);
   subVsClassfrVsFeatureAccuracy(:,:,i) = runClassifierOnSingleSubject(f_fullpath, classifiers);
end
%subVsClassfrVsFeatureAccuracy
   % avgAccuracy = mean(accuracy);
end

