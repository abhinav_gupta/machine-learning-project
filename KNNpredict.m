% Author: Sourav Kumar
% Email: skumar@cs.stonybrook.edu

function error = KNNpredict(traindata, trainlabels, testlabels, K, dist_mat)
	prediction = zeros(size(testlabels,1),K);
    error = zeros(1,K);
	for i = 1:size(testlabels)
			prediction(i, :) = KNNpredictPerTest(trainlabels, K, ...
                dist_mat(:, i));
	end
	for i = 1:K
			error(i) = ...
                sum(logical(prediction(:, i) ~= testlabels))/size(testlabels,1);
	end
end

function k_nearest_labels = KNNpredictPerTest(trainlabels, K, dist)
    % For K nearest Labels.
    k_nearest_labels = zeros(1, K);
    % sort the distance vector.
	[l, ind] = sort(dist, 'ascend');
	
	for i = 1:K
		k_labels = trainlabels(ind(1:min(i, end)));
		k_nearest_labels(i) = mode(k_labels);
    end
end