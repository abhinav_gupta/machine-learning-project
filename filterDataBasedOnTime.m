function [ f_info, f_data, f_meta ] = filterDataBasedOnTime( info,data,meta,time_window )
  ntrials= length(data);
  f_data = cell(ntrials,1);  
  f_meta=meta;
  
  for j=1:1:ntrials
    f_data{j} = data{j}(time_window,:);
    f_info(j) = info(j);
    f_info(j).len = length(time_window);
  end;
  f_meta.ntime_window= sum([f_info.len]);

end

