%there are three variance type: classDepVariance,
%classIndVariance,unitVariance
function [ model ] = trainNBClassifier( examples,labels, varianceType )
    nfeatures = size(examples, 2);
    sortedUniqLabels = sort(unique(labels));
    totalInstances = size(examples, 1);
    nClasses = length(sortedUniqLabels);
    classPriors = size(nClasses, 1);
    instancePerClass = size(nClasses, 1);

    for i=1:nClasses
        curLabel = sortedUniqLabels(i);
        curLabelInd = find(labels == curLabel);
        means(i, :) = mean(examples(curLabelInd, :), 1);
        switch varianceType
            case {'nb_classDepVar'}
                stds(i, :) = std(examples(curLabelInd, :),0, 1);
            case {'nb_classIndVar'}
                stds(i, :) = std(examples, 0,1);
            case {'unitVariance'}
                stds(i,:)  = ones(1,nfeatures);
        end
        instancePerClass(i) = length(curLabelInd);

    end
classPriors = instancePerClass/totalInstances;
classifier = cell(3,1);
classifier{1} = means;
classifier{2} = stds;
classifier{3} = classPriors;
model = classifier;
