function [predictions] = applyTANClassifier(dataset, model)
%dataset = dataset * -1;
numDataInst = size(dataset, 1);
numFeature = size(dataset, 2);
means = model{1};
stdev = model{2};
condCovM = model{3};
priors = model{4};
parentNodes = model{5};
nClasses = length(priors);

parentDependence = zeros(nClasses, numFeature);
parentSigmaDependence = zeros(nClasses, numFeature);

for n = 1:numDataInst
    for i = 1: nClasses
        condCov = condCovM{i};
        std = stdev;
        for feature = 1:numFeature
            parentFeature = parentNodes(feature);
            if parentFeature == 0
                parentDependence(i,feature) = 0;
                parentSigmaDependence(i,feature) = 1;
            else
                parentNode = dataset(n, parentFeature);
                parentDependence(i,feature) = condCov(feature, parentFeature);
                parentDependence(i,feature) = parentDependence(i,feature)/ ...
                    (std(i, parentFeature) .^ 2);
                parentDependence(i,feature) = parentDependence(i,feature) * ...
                    (parentNode - means(i, parentFeature));
                %Calculate sigma
                parentSigmaDependence(i,feature) = condCov(feature, parentFeature);
                parentSigmaDependence(i,feature) = parentSigmaDependence(i,feature) /...
                    (std(i, feature) * std(i, parentFeature));
                parentSigmaDependence(i,feature) = ...
                    sqrt( 1 - (parentSigmaDependence(i,feature).^ 2));
            end
        end
        meanPerClassPerInst = means(i, :);
        stdevPerClassPerInst = stdev(i, :);
        parentDependencePerClass = parentDependence(i, :);
        parentSigmaDependencePerClass = parentSigmaDependence(i, :);
        meanPerClassPerInst = meanPerClassPerInst + parentDependencePerClass;
        stdevPerClassPerInst = stdevPerClassPerInst .* parentSigmaDependencePerClass;
%         meanForAllInst = repmat(meanPerClassPerInst, [numDataInst, 1]);
%         stdevForAllInst = repmat(stdevPerClassPerInst, [numDataInst, 1]);
%         parentDependenceForAllInst = repmat(parentDependencePerClass, ...
%             [numDataInst, 1]);
%         parentSigmaDependenceForAllInst = repmat(parentSigmaDependencePerClass, ...
%             [numDataInst, 1]);
        
        repData = dataset(n, :);
        repData = repData - meanPerClassPerInst;
        repData = repData ./stdevPerClassPerInst;
        repData = -1 * (repData .^ 2);
        repData = repData / 2;
        repData = repData - log(sqrt(2*pi)*stdevPerClassPerInst);
        logMLE = sum(repData, 2);
        posterior(n,i) = logMLE + log(priors(i));
    end
end
[val, score] = max(posterior, [], 2);
predictions = score;
end