function [ model ] = trainTAN( dataset, labels )
    nfeatures = size(dataset, 2);
    % cov_full = cov(dataset);
    sortedUniqLabels = sort(unique(labels));
    nClasses = length(sortedUniqLabels);
    totalInstances = size(dataset, 1);
    classPriors = size(nClasses, 1);
    instancePerClass = size(nClasses, 1);
    condCovM = cell(nClasses, 1);
    means = zeros(nClasses, nfeatures);
    stds = zeros(nClasses, nfeatures);
    for i=1:nClasses
        curLabel = sortedUniqLabels(i);
        curLabelInd = find(labels == curLabel);
        filtEx = dataset(curLabelInd, :);
        condCovM{i} = cov(filtEx);
        means(i, :) = mean(filtEx, 1);
        stds(i, :) = std(filtEx,0, 1);
        instancePerClass(i) = length(curLabelInd);
    end
    classPriors = instancePerClass/totalInstances;
    corrCoeff = zeros(nfeatures, nfeatures);
    info_gain_per_class = zeros(nfeatures, nfeatures);
    infoGain = cell(nClasses, 1);
    info_gain_final = zeros(nfeatures, nfeatures);
    for c = 1:nClasses
        covM = condCovM{c};
        for i = 1: nfeatures
            for j = 1:nfeatures
                if i ~= j
                    corrCoeff(i, j) = covM(i,j)./(stds(c, i)*stds(c,j));
                    info_gain_per_class(i,j) = -1/2*log(1-corrCoeff(i, j)^2 );
                end
            end
        end
        %calculate information gain
        info_gain_final = info_gain_final + (info_gain_per_class.*classPriors(c));
        infoGain{c} = info_gain_per_class;
    end

    %Do the tree traversal:
    parentNodes  = maxSpanningTree(info_gain_final);

    classifier = cell(5,1);
    classifier{1} = means;
    classifier{2} = stds;
    classifier{3} = condCovM;
    classifier{4} = classPriors;
    classifier{5} = parentNodes;
    model = classifier;
    %     info_gain_final = info_gain_final*(1/nclasses);
end