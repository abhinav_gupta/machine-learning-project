function [ predictions ] = classifyTAN( dataset, model )
numDataInst = size(dataset, 1);
means = model{1};
stdev = model{2};
priors = model{3};
parentNodes = model{4};
nClasses = length(priors);
for i = 1: nClasses
    meansPerClass = means(i, :);
    stdevPerClass = stdev(i, :);
    %Replicate the means and stddev as many times as many data
    %instances are there
    meanForAllInst = repmat(meansPerClass, [numDataInst, 1]);
    stdevForAllInst = repmat(stdevPerClass, [numDataInst, 1]);
    repData = dataset;
    logMLE = zeros(numDataInst, 1);
    % this is also equal to number of nodes in the graph
    for f = 1:length(parentNodes)
        %Parent feature
        cf = f;
        %This tells the value of ith nodes parent.
        paf = parentNodes(f);
        repData = dataset(:, cf);
        %Current feature
        dataForCurF = dataset(:, cf);
        curFMean = meanForAllInst(:,cf);
        curFStd = stdevForAllInst(:,cf);
        %default initialization for newmean and new std
        newMean = curFMean;
        newStd = curFStd;
        %Check if the parent exists
        if(paf ~= 0)
            dataForParentF = dataset(:, paf);
            % Convert it for all data instances in that feature
            covM = cov(dataForCurF, dataForParentF);
            corrCoeff = covM/(curFMean*stdevForAllInst(:,paf));
            a = covM/(stdevForAllInst(:,paf).^2);
            %curFMean should have instances equal to the data instances
            newMean = curFMean + a.* dataForParentF;
            newVar = curFStd.^2 .*(1 - corrCoeff.^2);
            newStd = sqrt(newVar);
        end
        repData = repData - newMean;
        repData = repData ./newStd;
        repData = -1 * (repData .^ 2);
        repData = repData / 2;
        repData = repData - log(sqrt(2*pi)*newStd);
        logMLE = logMLE + repData;
        
    end
    posterior(:,i) = logMLE + priors(i);
end
[val, score] = max(posterior, [], 2);
predictions = score;


end

