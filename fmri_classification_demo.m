addpath tan
%classifiers.features = ['all_feature', 'active_roi', 'active_roi_avg', 'avg_voxel'];
classifiers.features = {'active_roi'};
%classifiers.features = {'all_feature'};
%Pass the value of C for svm
classifiers.svm = [10^2];
%Pass the value of K for knn
classifiers.knn = [1 2 4];
%nb-classIndVar, nb_classDepVar
%classfiers.type = ['nb_classIndVar', 'nb_classDepVar', 'knn', 'svm', 'tan'];
 classifiers.type = {'nb_classDepVar', 'tan'};
%classifiers.type = {'nb_classIndVar', 'nb_classDepVar'};
%its a 3d matrix, x: classifiers, y: features, z: subjects
accuracyMatrix = runClassifierOnAllSubjects('data_set', classifiers);
%accuracyMatrix = runClassifierOnSingleSubject('data_set\data-starplus-04847-v7.mat', classifiers);
nclassfrs = size(accuracyMatrix, 1);
nfeatures = size(accuracyMatrix, 2);
nsubjects = size(accuracyMatrix, 3);
accuracyMatrix
avgClassfrFeatureMatrix = mean(accuracyMatrix, 3);
for i = 1:nclassfrs
    fprintf('result for classifier %s\n', classifiers.type{i});
    avgClassfrFeatureMatrix(i, :)
end
