% addpath('C:\Users\gabhinav\Desktop\sbu\spring2013\ML\project\fmri_project');
[examples, labels] = generateFinalExamplesAndLabels('data_set\data-starplus-04847-v7.mat', 'all-feature');
[examples] = generateExamplesByAveragingAllImages(examples);
nFolds = 80;
%c = cvpartition(labels, 'Kfold',nFolds);
%For SVM:slack penalty
C = 10^5;
%c = cvpartition(labels,'leaveout');
c = cvpartition(labels, 'Kfold', nFolds);
for i = 1:c.NumTestSets
    trIdx = c.training(i);
    tsIdx = c.test(i);
    trX = examples(trIdx,:);
    trY = labels(trIdx);
    tsX = examples(tsIdx,:);
    tsY = labels(tsIdx);
    
    % train a Naive Bayes classifier
    %there are three variance type: classDepVariance, classIndVariance,unitVariance
%     [classifier] = trainNBClassifier(trX,trY,'classIndVariance');   %train classifier
%     [predictions] = applyNBClassifier(tsX,classifier);       %test it
%     nbAccuracy(i) = summarizeResults(predictions, tsY);
%     % accuracy(i)
    %====================SVM classifier===============%
    svmModel = trainSVM(trX,trY,C);
    %SVMStruct = svmtrain(trX, trY);
    svmpredictions = classifySVM(svmModel,tsX);
    %predictedLabels = classifySVM(svmModel,tsX);
    svmAccuracy(i) = summarizeResults(svmpredictions, tsY);
%     cv_mat_error(i) = ...
%             sum(abs(svmpredictions - tsY'))/length(tsY);
end
%nbScore = mean(nbAccuracy);
svmScore = mean(svmAccuracy);
%nbScore
svmScore
%
% % summarize the results of the above predictions.
%
%  [result,predictedLabels,trace] = summarizePredictions(predictions,classifier,'averageRank',labels);
%  1-result{1}  % rank accuracy
