 classifiers were evaluated by their cross-validated classification error when learning
boolean-valued classification functions. When m

classifier can be trained for the human subjects that were not the part of the training set.
================TODO========
==> DO nested cross validation for features.
multiple subject classifier and its comparison to single subject
feature selection:
- atcive :roiavg(mean of voxels in given set of ROIs), 
- merge all the voxels of different time and take a mean.
-  t-state.
- descriminative


run or different subjects and output the results.
Mention challenges.

Reason for bad results:
it is likely that the data for the worst-performing classifiers were
corrupted by various kinds of noise (e.g., significant head motion during imaging).

*These classifiers were selected because they have been used successfully
in other applications involving high dimensional data.

change variance and bias to generate class dependnce or in
GNB-SharedVariance for the Syntactic Ambiguity study, and GNB-DistinctVariance for the other two studies.


we can consider ROIs to remove restriction on brain size.
** use distinct variance.
** do something using the fixation data.
** Draw some plots for overfitting.

Feature selection:

we score each feature based on hw active it is during the class 1 or class 2 intervals, relative to the
fixation intervals. signal to noise ration.

we group the features involving the same voxel together for
the purpose of feature selection, and thus focus on selecting a subset of voxels.

One situation in which we might expect activity-based feature selection to outperform
discrimination-based methods is when data dimensionality is very high, noise levels are high,
training data are sparse, and very few voxels contain a signal related to the target classes.

Which risk is greater depends
on the exact problem, but the relative risk for the discrimination-based method grows more
quickly with increasing data dimension, increasing noise level, decreasing training set size,
and an increasing fraction of irrelevant features.

The
accuracy of this single-voxel classifier over the training data is taken as a measure of the
discriminating power of the voxel, and the n voxels that score highest according to this
measure are selected.

=========flow

results
analysis

================Report layout==============
Data preperation
Naive bayes.: classIndependentVariance and classIndependentVariance and unit variance

Reducing the number of features.
Details about the cross validation.
This analysis is not cross subjects.
===============Future work

Extend it for multiple subjects.



fMRI data is continuous, extremely high dimensional, sparse, and noisy. The learning
scheme used to analyze these data must take into account such characteristics.