function [ f_info, f_data, f_meta ] = filterDataBasedOnTrails( info,data,meta,trials )
 ntrials=length(trials);
  
  f_data = cell(ntrials,1);  
  f_meta=meta;
  for j=1:1:ntrials
    t=trials(j);  % get trial number
    f_data{j} = data{t};
    f_info(j) = info(t);
  end;
  f_meta.nsnapshots= sum([f_info.len]);
  f_meta.ntrials=length(trials);
  % update mint and maxt
  start_time = 1;
  for j=1:f_meta.ntrials
    end_time = start_time + f_info(j).len - 1;
    f_info(j).mint = start_time;
    f_info(j).maxt = end_time; 
    start_time = end_time + 1;
  end;



