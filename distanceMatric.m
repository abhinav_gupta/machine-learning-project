% Author: Sourav Kumar
% Email: skumar@cs.stonybrook.edu
function dist_mat = distanceMatric(traindata, testdata, dist_Func_Handler)
    train_size = size(traindata, 1);
    test_size = size(testdata, 1);
    dist_mat = zeros(train_size, test_size);
    for j=1:test_size
        for i=1:train_size
            dist_mat(i,j) = dist_Func_Handler(traindata(i,:), testdata(j,:));
        end
    end
end
