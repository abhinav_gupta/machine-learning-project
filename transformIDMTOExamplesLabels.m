%transform info,meta, data to a set examples and labels that can be used
%with the classifier.
function [examples,labels,expInfo] = transformIDMTOExamplesLabels( info, data, meta )
  % gather meta data about number,type and length of trials
  ntrials=length(info);
  nvoxels=size(data{1},2);
  trials=[info.cond];
  uniqueConds=sort(unique(trials));
  nconds=length(uniqueConds);
  trialLenCond=zeros(ntrials,nconds)+Inf;
  ntrialsCond=zeros(1,nconds);
  for c=1:ntrials
      tmp=find(uniqueConds==trials(c));
      trialLenCond(c,tmp)=size(data{c},1);
      ntrialsCond(tmp)=ntrialsCond(tmp)+1;
  end
  minTrialLenCond=min(trialLenCond,[],1);

  minTrialLen = min(minTrialLenCond);
  nfeatures = minTrialLen * nvoxels;
  nexamples = ntrials;  

  examples  = zeros(nexamples,nfeatures);
  labels    = trials';

  expInfo.experiment   = '';
  expInfo.dataType     = '';
  expInfo.meta         = meta;
  expInfo.featureToIDM = zeros(nfeatures,2);
  ftoidm1=repmat((1:nvoxels)',1,minTrialLen);
  ftoidm2=[];
  for m=1:minTrialLen
      ftoidm2(:,m)=ones(nvoxels,1)*m;
  end
  expInfo.featureToIDM(:,1)=reshape(ftoidm1,nfeatures,1);
  expInfo.featureToIDM(:,2)=reshape(ftoidm2,nfeatures,1);
  
  % 
  % Finally, create the examples
  %
  
  for j=1:ntrials
      tmpdata=data{j}(1:minTrialLen,:);
      tmpdata=reshape(tmpdata',nvoxels*minTrialLen,1);
      examples(j,:)=tmpdata';
  end
    