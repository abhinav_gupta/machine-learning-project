function dist = euclidianDistance(x,y)
dist = norm(x-y);
end