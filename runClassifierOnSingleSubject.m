function [ ClassfrVsFeatureAccuracy ] = runClassifierOnSingleSubject( file_name, classifiers )
% addpath('C:\Users\gabhinav\Desktop\sbu\spring2013\ML\project\fmri_project');
features = classifiers.features;
classfr_types = classifiers.type;
ntypes = length(classfr_types);
nfeatures = length(features);
ClassfrVsFeatureAccuracy = zeros(ntypes,nfeatures);

for fidx = 1:nfeatures
    %[examples, labels] = generateFinalExamplesAndLabels('data_set\data-starplus-04847-v7.mat', features{fidx});
    ftype = features{fidx};
    [examples, labels] = generateFinalExamplesAndLabels(file_name, ftype);
    
    if strcmp(ftype, 'avg_voxel') == 0
        [examples] = generateExamplesByAveragingAllImages(examples);
    end
    
    nFolds = 5;
    %c = cvpartition(labels, 'Kfold',nFolds);
    %For SVM:slack penalty
    %  C = 10^5;
    c = cvpartition(labels,'leaveout');
    accuracy = zeros(ntypes, c.NumTestSets);
    
    for i = 1:c.NumTestSets
        trIdx = c.training(i);
        tsIdx = c.test(i);
        trX = examples(trIdx,:);
        trY = labels(trIdx);
        tsX = examples(tsIdx,:);
        tsY = labels(tsIdx);
        
        for tidx = 1:ntypes
            classfr_type = classfr_types{tidx};
            switch classfr_type
                case {'nb_classIndVar', 'nb_classDepVar'}
                    %there are three variance type: classDepVariance,
                    %classIndVariance,unitVariance
                    [classifier] = trainNBClassifier(trX,trY,classfr_type);   %train classifier
                    [predictions] = applyNBClassifier(tsX,classifier);       %test it
                case {'tan'}
                    [model] = trainTAN(trX, trY);
                    [predictions] = applyTANClassifier(tsX, model);
                    %max_weighted_tree
                case {'knn'}
                    %FIll it later
                    K = classifiers.knn;
                    dist = dist_mat(trIdx, tsIdx);
                    
                    % KNN Prediction
                    cv_mat_error(i,:) = KNNpredict(trX, trY, tsY, K, dist);
                case {'svm'}
                    C = classifiers.svm;
                    svmModel = trainSVM(trX,trY,C);
                    predictions = classifySVM(svmModel,tsX);  
            end
            accuracy(tidx, i) = summarizeResults(predictions, tsY);
        end
    end
    
    meanAccuracy = mean(accuracy,2);
    %Feature vs classifier accuracy:
    ClassfrVsFeatureAccuracy(:, fidx) = meanAccuracy;
    
end
%ClassfrVsFeatureAccuracy
end

