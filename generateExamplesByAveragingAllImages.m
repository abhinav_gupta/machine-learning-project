function [new_examples] = generateExamplesByAveragingAllImages(examples)
%Get the mean of each voxel intensity across all the images.
    n_images = 16;
    n_features = size(examples, 2);
    n_instances = size(examples, 1);
    n_voxels = n_features/n_images;
    
    new_examples = zeros(n_instances, n_voxels);
    
    for i=1:n_images
        curIdx = ((i-1)*n_voxels +1):n_voxels*i;
        temp = examples(:, curIdx);
        new_examples = new_examples + temp;
    end
    new_examples = new_examples /16;
end